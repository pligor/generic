package com.pligor.generic

import java.util.Date

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
object GeoPosition {
  /**
   * use %lf if you want double instead of float
   */
  val googleMapsFormat = "%.6f,%.6f"
}

case class GeoPosition(latitude: Double, longitude: Double) {
  def toUTCoffsetInHours = TimeHelper.toUTCoffsetInHours(longitude)

  def itsUTCsunrise(date: Date = new Date) = TimeHelper.getUTCsunrise(this, date)

  def itsUTCsunset(date: Date = new Date) = TimeHelper.getUTCsunset(this, date)

  /**
   * Google Maps format
   * @return
   */
  override def toString: String = {
    GeoPosition.googleMapsFormat.format(latitude, longitude)
  }
}
