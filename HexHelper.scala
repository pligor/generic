package com.pligor.generic

/**
 * Created with IntelliJ IDEA.
 * User: pligor
 */
object HexHelper {
  val hexNumberRegex = "^[0-9a-fA-F]+$".r;

  def valueOf(buf: Array[Byte]): String = buf.map(x => f"$x%02X").mkString

  def containsOnly(str: String): Boolean = {
    hexNumberRegex.findFirstIn(str).isDefined
  }
}
