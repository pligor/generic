package com.pligor.generic

/**
 * http://stackoverflow.com/questions/18557854/trying-to-implement-a-circular-enumeration-in-scala
 */
trait CircularEnumeration extends Enumeration {

  def nextValue(value: Value): CircularEnumeration.this.Value = {
    //first way
    val nextIndex = values.toSeq.indexOf(value) + 1;
    val value_nomath = if (nextIndex >= values.size) {
      values.toSeq(0);
    }
    else {
      values.toSeq(nextIndex);
    }

    //second way
    val value_withmath = this((value.id + 1) % values.size);

    assert(value_nomath == value_withmath);

    value_withmath.asInstanceOf[CircularEnumeration.this.Value];
  }
}

/*
object MyEnum extends Enumeration with CircularEnumeration {
	val ZERO = Value;
	val ONE = Value;
	val TWO = Value;
}

val zero = MyEnum.ZERO;
println(zero);

val nextone = MyEnum.nextValue(zero);
println(nextone);

val andnext = MyEnum.nextValue(nextone);
println(andnext);

val gogo = MyEnum.nextValue(andnext);
println(gogo);
*/
