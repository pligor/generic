package com.pligor.generic

import java.io.{OutputStream, FileInputStream, File}
import com.pligor.generic.JavaStreamHelper._

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
class FileSender(private val file: File) {
  lazy val fileLen = file.length();

  //just for informative reasons
  private var sentByteCounter = 0;

  def getSentByteCounter = sentByteCounter;

  /**
   * @return from 0 to 1
   */
  def getPercentageSent = sentByteCounter.toFloat / fileLen.toFloat;

  //according to this: http://stackoverflow.com/questions/3122422/usage-of-bufferedinputstream
  //my read patterns suggest that using BufferedInputStream is NOT an optimization
  //private val bis = new BufferedInputStream(new FileInputStream(file), bufferByteLength);
  private val fileInputStream = new FileInputStream(file);

  def readNextBuffer(bufferByteLength: Int): Option[SendBuffer] = {
    val buffer = new Array[Byte](bufferByteLength);
    val noOfBytesOrMinusOne = fileInputStream.read(buffer);
    if (noOfBytesOrMinusOne == -1) {
      None;
    } else {
      sentByteCounter += noOfBytesOrMinusOne;
      Some(new SendBuffer(buffer.take(noOfBytesOrMinusOne)));
    }
  }

  def sendToOutStream(outputStream: OutputStream, bufferByteLength: Int, flushEachChunk: Boolean = false)
                     (onPercentageSent: (Float) => Unit) = {
    copyInputToOutputImperative(fileInputStream, outputStream, bufferByteLength, flushEachChunk = flushEachChunk)({
      len =>
        sentByteCounter += len;
        onPercentageSent(getPercentageSent);
    });
  }

  def end(): Unit = {
    fileInputStream.close();
  }
}

class SendBuffer(val bytes: Array[Byte]) {
  def isHashValid(hashBytes: Array[Byte]): Boolean = {
    //http://stackoverflow.com/questions/5393243/how-do-i-compare-two-arrays-in-scala
    hashBytes.deep == Hash(bytes).toByteArray.deep;
  }

  def getLen = bytes.length;
}
