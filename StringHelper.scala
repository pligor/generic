package com.pligor.generic

import java.nio.charset.{CharacterCodingException, Charset}
import java.nio.ByteBuffer
import scala.io.Codec

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
object StringHelper {
  //val UTF8charset = Charset.forName("UTF-8")

  /**
   * http://www.cl.cam.ac.uk/~mgk25/ucs/examples/UTF-8-test.txt
   * http://stackoverflow.com/questions/14236923/how-to-validate-if-a-utf-8-string-contains-mal-encoded-characters
   */
  def isValidUTF8(bytes: Array[Byte]): Boolean = {
    try {
      Codec.UTF8.charSet.newDecoder().decode(ByteBuffer.wrap(bytes));
      true;
    } catch {
      case e: CharacterCodingException => false;
    }
  }
}
