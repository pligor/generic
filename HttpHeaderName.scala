package com.pligor.generic

/**
 * Created with IntelliJ IDEA.
 * User: pligor
 * http://en.wikipedia.org/wiki/List_of_HTTP_header_fields
 */
object HttpHeaderName extends Enumeration {
  val CONTENT_LANGUAGE = Value("Content-Language")

  val CONTENT_TYPE = Value("Content-Type")

  val CONTENT_LENGTH = Value("Content-Length")

  val MY_COUNTRY = Value("My-Country")

  val X_FORWARDED_FOR = Value("X-Forwarded-For") //apache real ip

  val X_REAL_IP = Value("X-Real-IP") //nginx real ip
}
