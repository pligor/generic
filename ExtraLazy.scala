package com.pligor.generic

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
class ExtraLazy[T](notYetEvaluatedValue: => T) {
  var isSet = false

  lazy val value: T = {
    isSet = true

    notYetEvaluatedValue
  }
}
