Generic Helper Classes that you will find useful in many situations.

Depends on the following libraries:
"commons-httpclient" % "commons-httpclient" % "3.1"

"com.typesafe.play" %% "play-json" % "2.2.0"

Used in the following projects:
[bman](http://bman.co)
[fcarrier](http://facebook.com/FcarrierApp)
