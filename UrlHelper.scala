package com.pligor.generic

import java.net.{InetAddress, URLEncoder, MalformedURLException, URL}
import scala.collection.immutable.ListMap
//import org.apache.commons.httpclient.util.URIUtil
import org.apache.http.client.utils.URIUtils

/**
 * Created with IntelliJ IDEA.
 * User: pligor
 *
 * import this at some time: https://github.com/NET-A-PORTER/scala-uri
 */
object UrlHelper {
  val plainUrlPrefix = "http://"

  val secureUrlPrefix = "https://"

  def grabQueryParamOption(queryParams: Map[String, Seq[String]], key: String): Option[String] = {
    queryParams.applyOrElse(key, {
      key: String =>
        Seq.empty[String]
    }).headOption
  }

  /**
   * As pointed out here: http://stackoverflow.com/questions/7348711/recommended-way-to-get-hostname-in-java
   * this is not the best solution. Try to get it from request .. if available!
   */
  def getHostname = InetAddress.getLocalHost.getHostName

  def isValidUrl(url: String) = {
    try {
      new URL(url)

      true
    }
    catch {
      case e: MalformedURLException => false
    }
  }

  def urlEncode(str: String): String = URLEncoder.encode(str, "UTF-8")

  /*def mapToQueryString(map: Map[String, String]): String = {
    mapToQueryString(map.map(x => x._1 -> Seq[String](x._2)))
  }*/

  /**
   * NOTE: we do not take the case where we have seq[string] as a second type, yes we are in a hurry right now
   * @return
   */
  def queryStringToMap(queryString: String): Option[Map[String, String]] = {
    val arraysOfArrays = queryString.split('&').map(_.split('='))

    if (arraysOfArrays.forall(x => x.length == 1 || x.length == 2)) {

      val elems = arraysOfArrays.map(x => {
        if (x.length == 1) {
          x.head -> ""
        } else {
          //length == 2
          x(0) -> x(1)
        }
      })
      Some(
        Map.apply(elems: _ *)
      )
    } else {
      None
    }
  }

  /*def mapToQueryStringNoArraysKeepOrder(listMap: ListMap[String, String])(encoder: String => String = URIUtil.encodeQuery): String = {
    val convertedMap = listMap.map(x => x._1 -> Seq(x._2))

    mapToQueryStringKeepOrder(convertedMap)(encoder)
  }

  def mapToQueryStringKeepOrder(listMap: ListMap[String, Seq[String]])(encoder: String => String = URIUtil.encodeQuery): String = {
    val listOfTuples = listMap.toList.flatMap(x => x._2.map(y => (x._1, y)))

    listOfTuples.mapConserve(x => encoder(x._1) + "=" + encoder(x._2)).mkString("&")
  }

  def mapToQueryStringNoArrays(map: Map[String, String])(encoder: String => String = URIUtil.encodeQuery): String = {
    mapToQueryString(
      map.map(x => x._1 -> Seq(x._2))
    )(encoder)
  }

  def mapToQueryString(map: Map[String, Seq[String]])(encoder: String => String = URIUtil.encodeQuery): String = {
    val listOfTuples = map.toList.flatMap(x => x._2.map(y => (x._1, y)))
    listOfTuples.mapConserve(x => encoder(x._1) + "=" + encoder(x._2)).mkString("&")
  }*/
}

