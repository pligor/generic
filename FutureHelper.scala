package com.pligor.generic

import scala.concurrent._
import java.util.concurrent.atomic.AtomicReference

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
object FutureHelper {

  /**
   * sample usage:
    val (f, cancelFunc) = interruptableFuture({
      future: Future[String] =>
      Logger info "gamiesai prin to sleep"
      Thread.sleep(5000)
      Logger info "gamiesai meta to sleep"

      future.onComplete({
        tryString =>
          Logger info "gamiesai mesa sto complete"
      })

      "hello"
    })

    future {
      Thread.sleep(1000)
      cancelFunc()
      Logger info "egine cancel to gamo future"
    }
   */

  def interruptableFuture[T](fun: Future[T] => T)(implicit ex: ExecutionContext): (Future[T], () => Boolean) = {
    val p = Promise[T]()
    val f = p.future
    val aref = new AtomicReference[Thread](null)
    p tryCompleteWith Future {
      val thread = Thread.currentThread
      aref.synchronized {
        aref.set(thread)
      }
      try fun(f) finally {
        val wasInterrupted = aref.synchronized {
          aref getAndSet null
        } ne thread
        //Deal with interrupted flag of this thread in desired
      }
    }

    (f, () => {
      aref.synchronized {
        Option(aref getAndSet null) foreach {
          _.interrupt()
        }
      }
      p.tryFailure(new CancellationException)
    })
  }
}
