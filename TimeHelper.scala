package com.pligor.generic

import java.util.{Calendar, TimeZone, Date}
import java.text.SimpleDateFormat
import scala.math._
import scala.concurrent.duration._
import com.pligor.generic.MathHelper._
import play.api.libs.json._
import play.api.libs.json.JsSuccess
import play.api.libs.json.JsNumber

/**
 * Created with IntelliJ IDEA.
 * User: pligor
 */
object TimeHelper {
  private val civil = 96
  //degrees
  private val nautical = 102
  //degrees
  private val astronomical = 108
  //degrees

  private val defaultZenith = nautical

  val MySQLTimestampFormat = "yyyy-MM-dd HH:mm:ss"

  implicit val dateJsonWrites = new Writes[Date] {
    def writes(date: Date): JsValue = JsNumber(date.getTime)
  }

  implicit val dateJsonReads = new Reads[Date] {
    def reads(json: JsValue): JsResult[Date] = json match {
      case JsNumber(x) => JsSuccess(new Date(x.toLongExact))
      case _ => JsError("")
    }
  }

  def getUTCtomorrowMidnight(date: Date, timezoneId: String) = getUTCmidnight(date, timezoneId, +1)

  /**
   * @return get the time the midnight occurs but shifted for UTC
   */
  def getUTCmidnight(date: Date, timezoneId: String, daysOffset: Int = 0): Date = {
    require(isValidTimezone(timezoneId))
    val timezone = TimeZone.getTimeZone(timezoneId)
    val cal = Calendar.getInstance(/*TimeZone.getTimeZone(timezoneId)*/)
    cal.setTime(clearTime(date)) //clear time gives midnight by default
    cal.add(Calendar.DAY_OF_YEAR, daysOffset)
    convDate2GMT(cal.getTime, timezone)
  }

  def getUTCtimeAfterDuration(duration: Duration, currentDate: Date = new Date): Date = {
    val cal = Calendar.getInstance()
    cal.setTime(currentDate)
    cal.add(Calendar.MILLISECOND, duration.toMillis.toInt)
    cal.getTime
  }

  def getDurationOfNextUTCtime(hourOfDay: Int, minute: Int, date: Date = new Date): Duration = {
    try {
      getDurationUntilUTCtime(hourOfDay, minute, daysOffset = 0, date = date)
    }
    catch {
      case e: IllegalArgumentException => getDurationUntilUTCtime(hourOfDay, minute, daysOffset = 1, date = date)
    }
  }

  def getDurationUntilUTCtime(hourOfDay: Int, minute: Int, daysOffset: Int = 0, date: Date = new Date): Duration = {
    //THE TWO LINES BELOW CAUSED A PROBLEM, NOT EXACTLY SURE WHY
    //val cal = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
    //cal.setTimeZone(TimeZone.getTimeZone("GMT")); //redundancy but better be sure :P
    val cal = Calendar.getInstance()

    cal.setTime(date)
    //println("heheheheh");
    //println(hourOfDay);
    //println(cal.getTime);
    cal.add(Calendar.DAY_OF_YEAR, daysOffset)
    //println(cal.getTime);
    cal.set(Calendar.MILLISECOND, 0)
    //println(cal.getTime);
    cal.set(Calendar.SECOND, 0)
    //println(cal.getTime);
    cal.set(Calendar.MINUTE, minute)
    //println(cal.getTime);
    cal.set(Calendar.HOUR_OF_DAY, hourOfDay); //AND HERE WE GOT THE WRONG TIME

    val future = cal.getTime
    //println(cal.getTime);
    val futureMillis = future.getTime

    val dateMillis = date.getTime

    require(dateMillis < futureMillis)

    (futureMillis - dateMillis).milliseconds
  }

  def toUTCoffsetInHours(longitude: Double): Double = {
    val degreesOfFullCircle = 360

    val hoursInADay = 24 //1 day is a full rotation

    longitude / (degreesOfFullCircle / hoursInADay)
  }

  //astronomical constant, in degrees

  //http://williams.best.vwh.net/sunrise_sunset_algorithm.htm
  /**
   *
   * @param pos position
   * @param date a day (hours, minutes, seconds are ignored)
   * @param zenith offical = 90 degrees 50' (<=> 90 + 50/60), civil = 96 degrees, nautical = 102 degrees, astronomical = 108 degrees
   * @return UTC time
   */
  def getUTCsunrise(pos: GeoPosition, date: Date = new Date, zenith: Double = defaultZenith): Option[Date] = {
    getSunriseOrSunset(
      pos,
      date,
      zenith,
      factorForApproximateTime = 6,
      calculateH = (x: Double) => {
        360 - rad2deg(acos(x))
      }
    )
  }

  def getUTCsunset(pos: GeoPosition, date: Date = new Date, zenith: Double = defaultZenith): Option[Date] = {
    getSunriseOrSunset(
      pos,
      date,
      zenith,
      factorForApproximateTime = 18,
      calculateH = (x: Double) => {
        rad2deg(acos(x))
      }
    )
  }

  private def getSunriseOrSunset(
                                  pos: GeoPosition,
                                  unpureDate: Date,
                                  zenith: Double,
                                  factorForApproximateTime: Int,
                                  calculateH: (Double) => Double): Option[Date] = {
    val date = clearTime(unpureDate)

    //first calculate the day of the year
    val N = getYearDay(date)

    //convert the longitude to hour value
    val lngHour = pos.toUTCoffsetInHours

    //calculate an approximate time
    val t = N + (factorForApproximateTime - lngHour) / 24

    //calculate the Sun's mean anomaly
    val M = 0.9856 * t - 3.289

    def adjustment(x: Double, maximum: Double = 360, minimum: Double = 0): Double = {
      if (x < minimum) x + maximum
      else if (x > maximum) x - maximum
      else x
    }

    //calculate the Sun's true longitude
    //NOTE: L potentially needs to be adjusted into the range [0,360) by adding/subtracting 360
    val L = adjustment(M + (1.916 * sin(deg2rad(M))) + (0.020 * sin(deg2rad(2 * M))) + 282.634)

    //calculate the Sun's right ascension
    val RAtemp = adjustment(rad2deg(atan(0.91764 * tan(deg2rad(L)))))

    //right ascension value needs to be in the same quadrant as L
    val Lquadrant = floor(L / 90) * 90
    val RAquadrant = floor(RAtemp / 90) * 90
    val RA = toUTCoffsetInHours(RAtemp + (Lquadrant - RAquadrant))

    //calculate the Sun's declination
    val sinDec = 0.39782 * sin(deg2rad(L))
    val cosDec = cos(deg2rad(rad2deg(asin(sinDec)))); //OBVIOUSLY some redudancy here but it is a good reminder

    //calculate the Sun's local hour angle
    val cosH = (cos(deg2rad(zenith)) - (sinDec * sin(deg2rad(pos.latitude)))) / (cosDec * cos(deg2rad(pos.latitude)))
    if (cosH > 1 || cosH < -1) {
      //the sun never rises OR never sets on this location on the specified date
      return None
    }

    //finish calculating H and convert into hours
    val H = toUTCoffsetInHours(calculateH(cosH))

    //calculate local mean time of rising/setting
    val T = H + RA - (0.06571 * t) - 6.622

    //adjust back to UTC
    val UT = adjustment(T - lngHour, maximum = 24)

    val (hours, minutes, seconds, milliseconds) = decimalHour2hoursMinsSecsMsecs(UT)

    val gmtTimezone = TimeZone.getTimeZone("GMT")

    val gmtCal = Calendar.getInstance(gmtTimezone)

    gmtCal.setTimeZone(gmtTimezone)

    gmtCal.setTime(date)

    gmtCal.add(Calendar.HOUR, hours)

    gmtCal.add(Calendar.MINUTE, minutes)

    gmtCal.add(Calendar.SECOND, seconds)

    gmtCal.add(Calendar.MILLISECOND, milliseconds)

    Some(gmtCal.getTime)
  }

  /**
   * we dont really care about timezone here
   * @param date any date
   * @return
   */
  def clearTime(date: Date): Date = {
    /*val cal = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
    cal.setTimeZone(TimeZone.getTimeZone("GMT"));*/
    val cal = Calendar.getInstance()
    cal.setTime(date)
    cal.set(Calendar.HOUR_OF_DAY, 0)
    cal.set(Calendar.MINUTE, 0)
    cal.set(Calendar.SECOND, 0)
    cal.set(Calendar.MILLISECOND, 0)
    //println(cal.get(Calendar.HOUR_OF_DAY))

    cal.getTime
  }

  def decimalHour2hoursMinsSecsMsecs(x: Double) = {
    val hours = x.toInt

    val temp = (x - hours) * 60

    val minutes = temp.toInt

    val temp2 = (temp - minutes) * 60

    val seconds = temp2.toInt

    val milliseconds = ((temp2 - seconds) * 1000).toInt

    (hours, minutes, seconds, milliseconds)
  }

  def getYearDay(date: Date = new Date()): Int = {
    new SimpleDateFormat("D").format(date).toInt
  }

  def getDateInValidFilenameFormat(date: Date = new Date): String = {
    new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss").format(date)
  }

  def isValidTimezone(timezoneId: String): Boolean = {
    TimeZone.getAvailableIDs.exists(_ == timezoneId)
  }

  /**
   * SPECIAL USAGE ONLY: If you have a certain time let's say 2 o'clock in a certain timezone and you want
   * the SAME time (another point in time) in the GMT timezone
   */
  def convDate2GMT(date: Date = new Date(), curTimeZone: TimeZone = TimeZone.getDefault, debug: Boolean = false): Date = {
    //println(TimeZone.getAvailableIDs.mkString("\n"));
    //val curTimeZone = TimeZone.getTimeZone("Europe/Rome");
    if (debug) println("I belong in this timezone: " + curTimeZone.getID)

    //apexw tade deuterolepta apo to 1/1/1970 GMT
    val msFromEpochGmt: Long = date.getTime
    if (debug) println("My distance from 1/1/1970 GMT is: " + (msFromEpochGmt / 1000.0))
    if (debug) println("my distance from GMT (in ath) is larger from someone in England because their time is only seven oclock")
    if (debug) println("but my time is ten oclock. so english people are closer to Epoch time than myself. their dist is smaller")

    val offsetFromUTC: Int = curTimeZone.getOffset(msFromEpochGmt)
    if (debug) println("the offset of my timezone from true GMT time is " + offsetFromUTC)
    if (debug) println("again for athens you see this is positive! because I am further away from Epoch time")
    if (debug) println("remember than a few hours before 1/1/1970 in England we already celebrated new years eve here!")
    if (debug) println("I mean the clock was ticking many times before the english have their own new years eve")

    val gmtCal = Calendar.getInstance(TimeZone.getTimeZone("GMT"))
    gmtCal.setTime(date)
    gmtCal.add(Calendar.MILLISECOND, -offsetFromUTC)
    if (debug) println("As you see if I subtract these seconds I get the real value. How much english people are away from Epoch")
    if (debug) println("They are these many msec away: " + (gmtCal.getTime.getTime / 1000.0))
    if (debug) println("Now the time for them is: " + gmtCal.getTime)

    gmtCal.getTime
  }

  /**
   * @deprecated
   * @return
   */
  def getYearDay_old(date: Date = new Date()): Int = {
    val Array(day, month, year) = new SimpleDateFormat("dd/MM/yyyy").format(date).split("/").map(_.toInt)
    val N1 = floor(275 * month / 9).toInt
    val N2 = floor((month + 9) / 12)
    val N3 = 1 + floor((year - 4 * floor(year / 4) + 2) / 3)
    (N1 - (N2 * N3) + day - 30).toInt
  }
}
