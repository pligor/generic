package com.pligor.generic

import java.security.{DigestInputStream, MessageDigest}
import java.io.{FileInputStream, File}
import com.pligor.generic.FileHelper._

/**
 * Created with IntelliJ IDEA.
 * User: pligor
 */

trait HashTrait {
  def toByteArray: Array[Byte];

  override def toString = HexHelper.valueOf(toByteArray);
}

/**
 * @param algorithm Available algorithms: MD5, SHA1 or SHA-1, etc.
 */
case class HashFile(private val file: File, private val algorithm: String = "MD5") extends HashTrait {
  private val bufferLen = 1024;

  //this is now (h mipws not ithela na pw?) working: //org.apache.commons.codec.digest.DigestUtils.md5uppercase(fileInputStream);

  val toByteArray = {
    val fileInputStream = new FileInputStream(file);
    val messageDigest = MessageDigest.getInstance(algorithm);
    val digestInputStream = new DigestInputStream(fileInputStream, messageDigest);
    useEachBuffer(digestInputStream, bufferLen) {
      buffer: Array[Byte] =>
        //nop
    }
    messageDigest.digest();
  }

  //TESTED that produces exactly the same results
  /*val toByteArray2 = {
    val messageDigest = MessageDigest.getInstance(algorithm);
    val fileInputStream = new FileInputStream(file);
    try {
      useEachBuffer(fileInputStream, bufferLen) {
        buffer: Array[Byte] =>
          messageDigest.update(buffer);
      }
    } finally {
      fileInputStream.close();
    }
    messageDigest.digest();
  }
  def toString2 = HexHelper.valueOf(toByteArray2);*/
}

object Hash {
  def md5uppercase(str: String) = HexHelper.valueOf(Hash(str).toByteArray)
}

/**
 * @param value An Array[Byte] or a Serializable or a String
 * @param algorithm Available algorithms: MD5, SHA1 or SHA-1, etc.
 */
case class Hash(private val value: Any, private val algorithm: String = "MD5") extends HashTrait {
  val toByteArray = MessageDigest.getInstance(algorithm).digest(
    value match {
      case x: Array[Byte] => x;
      case y: Serializable => y.toString.toCharArray.map(_.toByte);
      case z: String => z.toCharArray.map(_.toByte);
      case _ => throw new ClassCastException;
    }
  );
}
