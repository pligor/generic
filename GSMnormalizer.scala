package com.pligor.generic

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 * http://www.csoft.co.uk/sms/character_sets/gsm.htm
 */
object GSMnormalizer {

  private val map = Map(
    'α' -> 'A', 'ά' -> 'A', 'Α' -> 'A', 'Ά' -> 'A',
    'β' -> 'B', 'Β' -> 'B',
    'γ' -> 'Γ', //gamma kefalaio ws exei
    'δ' -> 'Δ', //delta kefalaio ws exei
    'ε' -> 'E', 'έ' -> 'E', 'Ε' -> 'E', 'Έ' -> 'E',
    'ζ' -> 'Z', 'Ζ' -> 'Z',
    'η' -> 'H', 'ή' -> 'H', 'Η' -> 'H', 'Ή' -> 'H',
    'θ' -> 'Θ', //thita kefalaio ws exei
    'ι' -> 'I', 'ί' -> 'I', 'Ι' -> 'I', 'Ί' -> 'I', 'ϊ' -> 'I', 'ΐ' -> 'I', 'Ϊ' -> 'I', //giota kefalaio me dialytika yparxei?
    'κ' -> 'K', 'Κ' -> 'K',
    'λ' -> 'Λ', //lamda kefalaio ws exei
    'μ' -> 'M', 'Μ' -> 'M',
    'ν' -> 'N', 'Ν' -> 'N',
    'ξ' -> 'Ξ', //ksi kefalaio ws exei
    'ο' -> 'O', 'ό' -> 'O', 'Ο' -> 'O', 'Ό' -> 'O',
    'π' -> 'Π', //pi kefalaio ws exei
    'ρ' -> 'P', 'Ρ' -> 'P',
    'σ' -> 'Σ', 'ς' -> 'Σ', //sigma kefalaio ws exei
    'τ' -> 'T', 'Τ' -> 'T',
    'υ' -> 'Y', 'ύ' -> 'Y', 'Υ' -> 'Y', 'Ύ' -> 'Y', 'ϋ' -> 'Y', 'ΰ' -> 'Y', 'Ϋ' -> 'Y',
    'φ' -> 'Φ', //fi kefalaio ws exei
    'χ' -> 'X', 'Χ' -> 'X',
    'ψ' -> 'Ψ', //psi kefalaio ws exei
    'ω' -> 'Ω', 'ώ' -> 'Ω' //omega kefalaio ws exei
  )

  private val GSMsymbols = Seq(
    '@', 'Δ', ' ', '0', '¡', 'P', 'p',
    '£', '_', '!', '1', 'A', 'Q', 'a', 'q',
    '$', 'Φ', '"', '2', 'B', 'R', 'b', 'r',
    '¥', 'Γ', '#', '3', 'C', 'S', 'c', 's',
    'è', 'Λ', '¤', '4', 'D', 'T', 'd', 't',
    'é', 'Ω', '%', '5', 'E', 'U', 'e', 'u',
    'ù', 'Π', '&', '6', 'F', 'V', 'f', 'v',
    'ì', 'Ψ', '\'', '7', 'G', 'W', 'g', 'w',
    'ò', 'Σ', '(', '8', 'H', 'X', 'h', 'x',
    'Ç', 'Θ', ')', '9', 'I', 'Y', 'i', 'y',
    '\n', 'Ξ', '*', ':', 'J', 'Z', 'j', 'z',
    'Ø', 27.toChar, '+', ';', 'K', 'Ä', 'k', 'ä',
    'ø', 'Æ', '<', 'L', 'Ö', 'l', 'ö',
    '\r', 'æ', '-', '=', 'M', 'Ñ', 'm', 'ñ',
    'Å', '.', '>', 'N', 'Ü', 'n', 'ü',
    'å', 'É', '/', '?', 'O', '§', 'o', 'à'
  )

  assert(map.forall(c => GSMsymbols.exists(_ == c._2)),
    "all target characters must be gsm symbols")

  def apply(str: String) = {
    str.map((c: Char) => if (map.contains(c)) map(c) else c)
  }
}
