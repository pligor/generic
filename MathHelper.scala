package com.pligor.generic

import scala.math._

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
object MathHelper {
  def rad2deg(x: Double): Double = x * 180 / Pi

  def deg2rad(x: Double): Double = x * Pi / 180

  /**
   * f(n) = 2 power (2 power n) + 1
   * After n = 4 the number gets really big
   * http://en.wikipedia.org/wiki/Fermat_number
   * http://stackoverflow.com/questions/3087049/bouncy-castle-rsa-keypair-generation-using-lightweight-api
   * Number 65537 is used almost universally for RSA as a public exponent (yes I didnt understand much either)
   */
  val fermatNumbers = Set(3, 5, 17, 257, 65537)

  implicit def iterebleWithAvg[T: Numeric](data: Iterable[T]) = new {
    def avg = average(data)
  }

  def average[T](ts: Iterable[T])(implicit num: Numeric[T]) = {
    num.toDouble(ts.sum) / ts.size
  }

  def extractDouble(str: String): Double = {
    try {
      str.toDouble
    } catch {
      case e: NumberFormatException =>

        /*$value = self :: noWhitespace($value);

        if (strlen($value) == 0) {
          return false;
        }

        $matches = array(
          'point ' => array(),
      'comma ' => array(),
      );

      $pattern = array(
        'point ' => '@[-] ? ([ 0 - 9] +(, [0 - 9]
      {
        3
      }) *) (\.[ 0 - 9] +) ?@ ',
      'comma ' => '@[-] ? ([ 0 - 9] +(\.[ 0 - 9]
      {
        3
      }) *) (, [0 - 9] +) ?@ ',
      );

      $count = array();
      $result = array();

      $count[ 'point '] = preg_match($pattern[ 'point '], $value, $matches[ 'point '] );
      if ($count[ 'point '] == 1)
      {
        $result[ 'point '] = reset($matches[ 'point '] );
        $result[ 'point '] = str_replace(',', ' ', $result[ 'point '] );
      }

      $count[ 'comma '] = preg_match($pattern[ 'comma '], $value, $matches[ 'comma '] );
      if ($count[ 'comma '] == 1)
      {
        $result[ 'comma '] = reset($matches[ 'comma '] );
        $result[ 'comma '] = str_replace('.', ' ', $result[ 'comma '] );
        $result[ 'comma '] = str_replace(',', '.', $result[ 'comma '] );
      }

      $final = null;

      if (array_sum($count) == 0) {
        //nothing found
        return false;
      }
      elseif(array_sum($count) == 1) {
        if ($count[ 'point '] == 1)
        {
          $final = $result[ 'point '];
        }
        elseif($count[ 'comma '] == 1)
        {
          $final = $result[ 'comma '];
        }
      }
      elseif(array_sum($count) == 2) {
        if (strlen($result[ 'point '] ) > strlen ($result[ 'comma '] ) )
        {
          $final = $result[ 'point '];
        }
        else
        {
          $final = $result[ 'comma '];
        }
      }

      if (is_numeric($final)) {
        return floatval($final);
      }

      return false;*/
        0.0
    }
  }

  /*
  public function testParseFloat() {
		$func = lcfirst( substr(__FUNCTION__, strlen('test')) );
		$function = array($this->class, $func);

		$var = 1;
		$actual = call_user_func($function,$var);
		$this->assertEquals(1, $actual);

		$var = 1.0;
		$actual = call_user_func($function,$var);
		$this->assertEquals(1, $actual);

		$var = 1.1;
		$actual = call_user_func($function,$var);
		$this->assertEquals(1.1, $actual);

		$var = -1.1;
		$actual = call_user_func($function,$var);
		$this->assertEquals(-1.1, $actual);

		$var = '-361.00155';
		$actual = call_user_func($function,$var);
		$this->assertEquals(-361.00155, $actual);

		$var = 'sagregre-361.00155';
		$actual = call_user_func($function,$var);
		$this->assertEquals(-361.00155, $actual);

		$var = '361.00155ninin';
		$actual = call_user_func($function,$var);
		$this->assertEquals(361.00155, $actual);

		$var = '100.884.677.001,55 ';
		$actual = call_user_func($function,$var);
		$this->assertEquals(100884677001.55, $actual);

		$var = '100,884,677,001.55 ';
		$actual = call_user_func($function,$var);
		$this->assertEquals(100884677001.55, $actual);

		$var = ' ';
		$condition = call_user_func($function,$var);
		$this->assertFalse($condition);
	}
   */
}
