package com.pligor.generic

import com.pligor.generic.FileHelper._
import java.io.{InputStream, File, FileOutputStream}
import com.pligor.generic.JavaStreamHelper._

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
class FileReceiver(private val fileLength: Long, fileExt: Option[String]) {
  private var fileByteCounter = fileLength

  val fileRandomTemporaryPath: String = genRandomTemporaryFile(ext = fileExt).getCanonicalPath

  private val fileOutputStream = new FileOutputStream(getTempFile)

  private def getTempFile = new File(fileRandomTemporaryPath)

  def countBytesReceived = fileLength - fileByteCounter

  /**
   * @return from 0 to 1
   */
  def getPercentageReceived = countBytesReceived.toFloat / fileLength.toFloat

  def isFileReceivedSuccessfully = {
    isFileByteCounterZero && !isFileByteCounterAtBeginning
  }

  def cancel(): Unit = {
    closeFile()
    //do not care if actually deleted or not because cancel can be called many times
    val deleted = getTempFile.delete()
  }

  def write(inputStream: InputStream, bufferSize: Int)(onBufferWritten: (Int) => Unit) = {
    copyInputToOutputImperative(inputStream, fileOutputStream, bufferSize) {
      bufferLenWritten =>
        decreaseFileByteCounter(bufferLenWritten)
        onBufferWritten(bufferLenWritten)
    }
  }

  def write(bytes: Array[Byte]): Unit = {
    //try {
    fileOutputStream.write(bytes)
    decreaseFileByteCounter(bytes.length)
    /*} catch {
      case e: IOException => {
        e.printStackTrace();
        log log "io exception while writing to a temporary file ??!!";
      }
    }*/
  }

  def end(): File = {
    closeFile()
    val tempFile = getTempFile
    assert(tempFile.length() == fileLength); //we can close file either successfully or not
    //log log "we just wrote to " + fileRandomTemporaryPath;
    tempFile
  }

  def safeEnd(): Option[File] = {
    if (isFileReceivedSuccessfully) {
      Some(end())
    } else {
      cancel()
      None
    }
  }

  def closeFile(): Unit = {
    fileOutputStream.flush()

    fileOutputStream.close()
  }

  private def decreaseFileByteCounter(decrease: Int): Unit = {
    fileByteCounter -= decrease

    assert(fileByteCounter >= 0,
      "file byte counter reached this negative value: " + fileByteCounter)
  }

  private def isFileByteCounterZero: Boolean = fileByteCounter == 0

  private def isFileByteCounterAtBeginning: Boolean = fileByteCounter == fileLength
}
