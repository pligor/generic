package com.pligor.generic

/**
 * Created with IntelliJ IDEA.
 * User: pligor
 * http://www.freeformatter.com/mime-types-list.html
 */
object ContentType extends Enumeration {
  val PNG = Value("image/png")

  val JPEG = Value("image/jpeg")

  val APPLICATION_JSON = Value("application/json")

  val TEXT_PLAIN = Value("text/plain")

  def MULTIPART_FORM_DATA(boundary: String) = "multipart/form-data;boundary=" + boundary
}
